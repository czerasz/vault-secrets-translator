# Vault Secrets Translator

Replace `.` and `-` to `_` in Vault secret key names.

## Build

```bash
./scripts/build
```
