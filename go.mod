module gitlab.com/czerasz/vault-translator

go 1.16

require (
	github.com/hashicorp/vault/api v1.0.4
	github.com/kelseyhightower/envconfig v1.4.0
)
