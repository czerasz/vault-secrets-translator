package main

import (
	"errors"
	"log"
	"net/http"
	"path/filepath"
	"strings"
	"time"

	"github.com/hashicorp/vault/api"
	"github.com/kelseyhightower/envconfig"
)

var httpClient = &http.Client{
	Timeout: 5 * time.Second,
}

type Config struct {
	Address string `envconfig:"VAULT_ADDR" required:"true"`
	Token   string `envconfig:"VAULT_TOKEN" required:"true"`
	Path    string `envconfig:"SECRETS_PATH" required:"true"`
	DryRun  bool   `envconfig:"DRY_RUN" default:"true"`
}

func main() {
	var conf Config
	err := envconfig.Process("vault_translator", &conf)
	if err != nil {
		log.Fatal(err.Error())
	}

	c, err := api.NewClient(&api.Config{
		Address:    conf.Address,
		HttpClient: httpClient,
	})

	if err != nil {
		panic(err)
	}

	c.SetToken(conf.Token)

	secrets, err := list(c.Logical(), filepath.Join(conf.Path, "metadata"))

	for _, secret := range secrets {
		secretPath := filepath.Join(conf.Path, "data", secret)

		secretData, err := read(c.Logical(), secretPath)
		if err != nil {
			panic(err)
		}

		replace := false
		updatedSecret := make(map[string]string)
		for secretKey, secretValue := range secretData {
			newSecretKey := secretKey
			if strings.Contains(secretKey, ".") || strings.Contains(secretKey, "-") {
				replace = true
				newSecretKey = strings.ToUpper(strings.ReplaceAll(secretKey, ".", "_"))
				newSecretKey = strings.ToUpper(strings.ReplaceAll(secretKey, "-", "_"))

				log.Printf("replace %s: %s to %s", secretPath, secretKey, newSecretKey)
			}

			updatedSecret[newSecretKey] = secretValue
		}

		log.Printf("updated secret: %+v", updatedSecret)

		if replace && !conf.DryRun {
			data := make(map[string]interface{})
			data["data"] = updatedSecret
			_, err := c.Logical().Write(secretPath, data)
			if err != nil {
				panic(err)
			}
		}
	}
}

// list returns secret keys
func list(l *api.Logical, path string) ([]string, error) {
	keys := []string{}

	res, err := l.List(path)

	if err != nil {
		return keys, err
	}

	resKeys, ok := res.Data["keys"]
	if !ok {
		return keys, errors.New("no keys property")
	}

	keysRaw, ok := resKeys.([]interface{})
	if !ok {
		return keys, errors.New("reponse data can not be converted")
	}

	for _, keyRaw := range keysRaw {
		key, ok := keyRaw.(string)
		if !ok {
			return keys, errors.New("can not convert key")
		}

		keys = append(keys, key)
	}

	return keys, nil
}

// read returns secret data
func read(l *api.Logical, path string) (map[string]string, error) {
	secret := make(map[string]string)

	data, err := l.Read(path)
	if err != nil {
		return secret, err
	}

	resData, ok := data.Data["data"]
	if !ok {
		return secret, errors.New("no reponse data")
	}

	secretData, ok := resData.(map[string]interface{})
	if !ok {
		return secret, errors.New("reponse data can not be converted")
	}

	for secretKey, secretValueRaw := range secretData {
		secretValue, ok := secretValueRaw.(string)
		if !ok {
			return secret, errors.New("secret value can not be converted")
		}

		secret[secretKey] = secretValue
	}

	return secret, nil
}
